# ----------------------------------------------------------------------------#
# Imports
# ----------------------------------------------------------------------------

from flask import Flask, render_template, request, Response
import json
import requests
import time
from pprint import pprint
from flask_sqlalchemy import SQLAlchemy
import logging
from logging import Formatter, FileHandler
from forms import *
import os
from sqlalchemy.sql import table, column, select, update, insert
import models
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.figure import Figure
import matplotlib.pyplot as plt
import io
import random
from pylab import *
from datetime import datetime
from sqlalchemy.orm import sessionmaker

# ----------------------------------------------------------------------------#
# App Config.
# ----------------------------------------------------------------------------#

app = Flask(__name__)
app.config.from_object('config')
db = SQLAlchemy(app)


# Automatically tear down SQLAlchemy.

@app.teardown_request
def shutdown_session(exception=None):
    db.session.remove()


# Login required decorator.
'''
def login_required(test):
    @wraps(test)
    def wrap(*args, **kwargs):
        if 'logged_in' in session:
            return test(*args, **kwargs)
        else:
            flash('You need to login first.')
            return redirect(url_for('login'))
    return wrap
'''


# ----------------------------------------------------------------------------#
# Controllers.
# ----------------------------------------------------------------------------#


@app.route('/')
def home():
    return render_template('pages/placeholder.home.html')


@app.route('/about')
def about():
    return render_template('pages/placeholder.about.html')


@app.route('/login')
def login():
    form = LoginForm(request.form)
    return render_template('forms/login.html', form=form)


@app.route('/register')
def register():
    form = RegisterForm(request.form)
    return render_template('forms/register.html', form=form)


@app.route('/forgot')
def forgot():
    form = ForgotForm(request.form)
    return render_template('forms/forgot.html', form=form)


@app.route('/plot.png')
def plot_png():
    fig = create_figure()
    output = io.BytesIO()
    FigureCanvas(fig).print_png(output)
    return Response(output.getvalue(), mimetype='image/png')


def create_figure():
    Session = sessionmaker(bind=models.engine)
    session = Session()
    result = session.query(models.Sensor).all()
    fig = Figure()
    fig.set_size_inches(18.5, 10.5)
    axis = fig.add_subplot(1, 1, 1)

    for row in result:
        if row.sensor == '3130':
            y = array(row.temperature)
            x = array(row.date_create)
            axis.plot(x, y)

    return fig


@app.route('/datafetcher')
def datafetcher():
    response = requests.get("http://app.objco.com:8099/?account=16L1SPQZS3&limit=2")
    json_data = response.json()

    query = requests.get(
        "https://public.opendatasoft.com/api/records/1.0/search/?dataset=arome-0025-enriched&q=Bordeaux&lang=fr&rows=20&refine.commune=Bordeaux&refine.code_commune=33063")
    json_weather = query.json()

    commune = json_weather['records'][19]['fields']['commune']
    temp = json_weather['records'][19]['fields']['minimum_temperature_at_2_metres']
    temp_round = round(temp, 2)

    for data_object in json_data:
        hex_position = data_object[1].find("62182233");

        hex_found = data_object[1];
        position = 8
        hex_found = hex_found[hex_position:]
        ID = hex_found[0:position];
        status_dec = hex_found[position:position + 2]

        battery_hex = hex_found[position + 2:position + 6];
        battery_dec = str(int(battery_hex, 16))
        battery_dec = battery_dec[:1] + "," + battery_dec[1:]

        temperature_hex = hex_found[position + 6:position + 10];
        temperature_dec = str(int(temperature_hex, 16));
        temperature_dec = temperature_dec[:2] + "," + temperature_dec[2:]

        humidite_hex = hex_found[position + 10:position + 12];
        humidite_dec = str(int(humidite_hex, 16));

        rssi_hex = hex_found[position + 12:position + 14];
        rssi_dec = str(int(rssi_hex, 16));

        sensor = models.Sensor(
            sensor=str(data_object[0]),
            battery=str(battery_dec),
            status=str(status_dec),
            temperature=str(temperature_dec),
            rssi=str(rssi_dec),
            humidite=str(humidite_dec),
            date_create=str(datetime.now())
        );

        db.session.add(sensor)

        db.session.commit()
        sensors = models.Sensor.query.all();

    return render_template('pages/datafetcher.html', oldsensors=sensors, city=commune, temp_city=temp_round)


# Error handlers.


@app.errorhandler(500)
def internal_error(error):
    db.session.rollback()
    return render_template('errors/500.html'), 500


@app.errorhandler(404)
def not_found_error(error):
    return render_template('errors/404.html'), 404


if not app.debug:
    file_handler = FileHandler('error.log')
    file_handler.setFormatter(
        Formatter('%(asctime)s %(levelname)s: %(message)s [in %(pathname)s:%(lineno)d]')
    )
    app.logger.setLevel(logging.INFO)
    file_handler.setLevel(logging.INFO)
    app.logger.addHandler(file_handler)
    app.logger.info('errors')

# ----------------------------------------------------------------------------#
# Launch.
# ----------------------------------------------------------------------------#

# Default port:
if __name__ == '__main__':
    app.run()

# Or specify port manually:
'''
if __name__ == '__main__':
    port = int(os.environ.get('PORT', 5000))
    app.run(host='0.0.0.0', port=port)
'''

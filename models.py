from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String
from app import db

engine = create_engine('sqlite:///database.db', echo=True)
db_session = scoped_session(sessionmaker(autocommit=False,
                                         autoflush=False,
                                         bind=engine))
Base = declarative_base()
Base.query = db_session.query_property()

# Set your classes here.


class User(Base):
    __tablename__ = 'Users'
    __table_args__ = {'extend_existing': True}
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(120), unique=True)
    email = db.Column(db.String(120), unique=True)
    password = db.Column(db.String(30))

    def __init__(self, name=None, password=None):
        self.name = name
        self.password = password

class Sensor(Base):
    __tablename__ = 'Sensor'
    __table_args__ = {'extend_existing': True}
    id = db.Column(db.Integer, primary_key=True)
    sensor = db.Column(db.String(500))
    battery = db.Column(db.String(500))
    status = db.Column(db.String(500))
    temperature = db.Column(db.String(500))
    humidite = db.Column(db.String(500))
    rssi = db.Column(db.String(500))
    date_create = db.Column(db.String(500))

    def __init__(self, sensor=None, battery=None, status=None,  temperature=None, humidite=None, rssi=None, date_create=None):
        self.sensor = sensor
        self.temperature = temperature
        self.battery = battery
        self.status = status
        self.humidite = humidite
        self.rssi = rssi
        self.date_create = date_create


# Create tables.
Base.metadata.drop_all(bind=engine)
Base.metadata.create_all(bind=engine)
